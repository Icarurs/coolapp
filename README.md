# Cool Application

This is a sample application for [Park Bench Digital](http://parkbenchdigital.com/).

## Requirements

Consume data from a web API, and display it in an interesting manner. Specific technology areas:

- Ruby on Rails
- Authentication (Devise?)
- Paging (will_paginate?)
- User Interface (Bootstrap?)
- Unit tests